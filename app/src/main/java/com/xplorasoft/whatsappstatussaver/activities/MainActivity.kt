package com.xplorasoft.whatsappstatussaver.activities

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TabLayout
import cn.jzvd.JZVideoPlayer
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.xplorasoft.whatsappstatussaver.R
import com.xplorasoft.whatsappstatussaver.commoners.BaseActivity
import com.xplorasoft.whatsappstatussaver.fragments.ImagesFragment
import com.xplorasoft.whatsappstatussaver.fragments.SavedFragment
import com.xplorasoft.whatsappstatussaver.fragments.VideosFragment
import com.xplorasoft.whatsappstatussaver.utils.PagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : BaseActivity(), TabLayout.OnTabSelectedListener {
    private var doubleBackToExit = false
    lateinit var mAdView : AdView

    var img: String? = ""
    var vid: String? = ""
    var svd: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        img = resources.getString(R.string.mImages)
        vid = resources.getString(R.string.mVideos)
        svd = resources.getString(R.string.mSaved)

        initViews()

        if (!storagePermissionGranted()) requestStoragePermission()

        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
    }

    private fun initViews() {
//        setSupportActionBar(toolbar)
//        supportActionBar?.title = getString(R.string.app_name)

        setupViewPager()
        setupTabs()
    }

    private fun setupViewPager() {
        val adapter = PagerAdapter(supportFragmentManager, this)
        val images = ImagesFragment()
        val videos = VideosFragment()
        val saved = SavedFragment()

        adapter.addAllFrags(images, videos, saved)
        adapter.addAllTitles(img, vid, svd)

        viewpager.adapter = adapter
        viewpager.offscreenPageLimit = 2
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))

    }

    private fun setupTabs() {
        tabs.setupWithViewPager(viewpager)
        tabs.addOnTabSelectedListener(this)
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        viewpager.setCurrentItem(tab!!.position, true)
    }

    override fun onBackPressed() {
        if (JZVideoPlayer.backPress()) {
            return
        }

        if (doubleBackToExit) {
//            val keluar = Intent(this@MainActivity, Splash::class.java)
//            startActivity(keluar)
//            finish()
//            moveTaskToBack(true)
            super.onBackPressed()
        } else {
            toast("Please tap back again to exit")

            doubleBackToExit = true

            Handler().postDelayed({doubleBackToExit = false }, 1500)
        }
    }

    override fun onPause() {
        super.onPause()
        JZVideoPlayer.releaseAllVideos()
    }

}
