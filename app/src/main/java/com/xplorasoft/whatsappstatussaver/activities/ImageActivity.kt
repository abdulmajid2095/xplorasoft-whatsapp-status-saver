package com.xplorasoft.whatsappstatussaver.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.xplorasoft.whatsappstatussaver.R
import com.xplorasoft.whatsappstatussaver.commoners.K
import com.xplorasoft.whatsappstatussaver.models.Story
import com.xplorasoft.whatsappstatussaver.utils.loadUrl
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        val story = intent.getSerializableExtra(K.STORY) as Story
        image.loadUrl(story.path!!)
    }
}
