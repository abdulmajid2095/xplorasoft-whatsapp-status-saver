package com.xplorasoft.whatsappstatussaver.activities

import android.os.Bundle
import cn.jzvd.JZVideoPlayer
import cn.jzvd.JZVideoPlayerStandard
import com.xplorasoft.whatsappstatussaver.R
import com.xplorasoft.whatsappstatussaver.commoners.BaseActivity
import com.xplorasoft.whatsappstatussaver.commoners.K
import com.xplorasoft.whatsappstatussaver.models.Story
import com.xplorasoft.whatsappstatussaver.utils.loadUrl
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        val story = intent.getSerializableExtra(K.STORY) as Story

        video.setUp(story.path, JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, "")
        video.thumbImageView.loadUrl(story.path!!)
    }

    override fun onPause() {
        super.onPause()
        JZVideoPlayer.releaseAllVideos()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        JZVideoPlayer.backPress()
    }
}
