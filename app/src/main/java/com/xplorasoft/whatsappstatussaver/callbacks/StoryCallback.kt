package com.xplorasoft.whatsappstatussaver.callbacks

import android.view.View
import com.xplorasoft.whatsappstatussaver.models.Story

interface StoryCallback {

    fun onStoryClicked(v: View, story: Story)

}